﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;
using ConicalEffect.IsolatedStorage;

namespace ConicalEffect.IsolatedStorageXml
{
	public class XmlStorage
	{
		private IsoStorInterface _isolatedStorageInterface;

		public IsoStorInterface IsolatedStorageInterface {
			get { return _isolatedStorageInterface ?? (_isolatedStorageInterface = new IsoStorInterface()); }
			set { _isolatedStorageInterface = value; }
		}

		public T ReadXmlIntoObject<T>(string filename) where T : class 
		{
			using (var userStore = IsolatedStorageInterface.UserStore)
			{
				using (var stream = userStore.OpenFile(filename, FileMode.Open))
				{
					var serializer = new XmlSerializer(typeof(T));

					return serializer.Deserialize(stream) as T;
				}
			}
		}

		public void WriteXmlObjectToFile<T>(T entity, string filename)
		{
			var xmlSettings = new XmlWriterSettings { Indent = true };

			using (var userStore = IsolatedStorageInterface.UserStore)
			{
				using (var stream = userStore.OpenFile(filename, FileMode.Create))
				{
					var serializer = new XmlSerializer(typeof(T));

					using (var writer = XmlWriter.Create(stream, xmlSettings))
					{
						serializer.Serialize(writer, entity);
					}
				}
			}
		}

		public bool FileExists(string file)
		{
			using (var userStore = IsolatedStorageInterface.UserStore)
			{
				return userStore.FileExists(file);
			}
		}
	}
}
